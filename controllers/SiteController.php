<?php

namespace app\controllers;

use app\models\Portfolio;
use app\models\Publication;
use app\models\Sketchbook;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $images = Portfolio::find()->orderBy('position')->select(['src', 'id'])->all();
        return $this->render('index', ['images' => $images, 'folder' => 'portfolio']);
    }

    public function actionSketchbook()
    {
        $images = Sketchbook::find()->orderBy('position')->select(['src', 'id'])->all();
        return $this->render('index', ['images' => $images, 'folder' => 'sketchbook']);
    }

    public function actionPublication()
    {
        $images = Publication::find()->orderBy('position')->select(['src', 'desctiption'])->all();
        return $this->render('publication', ['images' => $images, 'folder' => 'publication']);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionContacts(){
        $model = new ContactForm();
        if($model->load(Yii::$app->request->post()) && $model->contact()){
            Yii::$app->session->setFlash('success', 'Message sent successfully');
            return $this->redirect('contacts');
        }
        return $this->render('contacts', ['model' => $model]);
    }

}
