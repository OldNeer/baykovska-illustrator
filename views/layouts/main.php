<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
header('Pragma: public');
header('Cache-Control: public, max-age=3600');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="icon" href="/img/ico.png" type="img/png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="gallery-bg">
</div>
<div class="navbuttns">
    <div class="next" onclick="nextimg()">
        <svg viewBox="0 0 180 310" role="button" aria-label="next">
            <path d="M10 10 L170 161 M170 150 L10 300"></path>
        </svg>
    </div>
    <div class="back" onclick="previmg()">
        <svg viewBox="0 0 180 310" role="button" aria-label="back">
            <path d="M170 10 L10 161 M10 150 L170 300"></path>
        </svg>
    </div>
    <svg viewBox="0 0 180 180" class="closeGlr" tabindex="0" role="button" aria-label="close">
        <path d="M5 5 L175 175 M175 5 L5 175"></path>
    </svg>
</div>
<div class="gallery">
    <div class="images">
    </div>
    <div class="desc"></div>
</div>
</div>
<!--HEADER Block: Begin-->
<header>
    <div id="normalize">-</div>
    <div id="menu">
        <li><a id="button" href="/">portfolio</a></li>
        <li><a id="button" href="/sketchbook">sketchbook</a></li>
        <li><a id="button" href="/publication">publication</a></li>
        <li><a id="button" href="/contacts">contacts</a></li>
        <li><a id="button" href="/about">about</a></li>
    </div>
    <button id="hide-show"><img src="/img/1231231.png" alt=""></button>
    <div class="logo">
        <a href="/"><img id="IRA" src="/img/logo.png"></a>
    </div>
    <ul class="navigationbar">
        <li><a id="button" href="/">portfolio</a></li>
        <li><a id="button" href="/sketchbook">sketchbook</a></li>
        <li><a id="button" href="/publication">publication</a></li>
        <li><a id="button" href="/contacts">contacts</a></li>
        <li><a id="button" href="/about">about</a></li>
        <li class="social"><a href="https://www.instagram.com/iraillustration/"><img src="/img/instagram.png"></a><a
                    href="https://www.facebook.com/baykovska/"><img src="/img/facebook.png"></a></li>
    </ul>
    <li class="social-mobile"><a href="https://www.instagram.com/iraillustration/"><img src="/img/instagram.png"></a><a
                href="https://www.facebook.com/baykovska/"><img src="/img/facebook.png"></a>
</header>
<main class="page-content">
    <div class="cnt">
        <div id="content">
            <?=Alert::widget()?>
            <?= $content ?>
        </div>
    </div>
</main>
<footer>
    <div class="main-footer">
        <div class="copy-right">
            <span>&copy;Ira Baykovska. All rights reserved.</span>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
