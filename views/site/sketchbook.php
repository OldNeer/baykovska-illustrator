<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$this->registerJsFile('js/gallery.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="items">
    <?php if (!empty($images)): ?>
        <?php foreach ($images as $image): ?>
            <?= Html::img('uploads/' . $image, ['onclick' => 'showimg(this)']) ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
