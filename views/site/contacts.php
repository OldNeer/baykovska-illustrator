<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use \yii\helpers\Html;

$this->title = Yii::$app->name;
$this->registerCssFile('/css/contacts.css')
?>
<img src="/img/contact.jpg">
<div class="contact-form">
    <?php $form = ActiveForm::begin() ?>
    <?=$form->field($model, 'name')->textInput(['placeholder' => 'Name', 'class' => 'form-input'])->label(false)?>
    <?=$form->field($model, 'email')->textInput(['placeholder' => 'E-mail', 'class' => 'form-input'])->label(false)?>
    <?=$form->field($model, 'subject')->textInput(['placeholder' => 'Subject', 'class' => 'form-input'])->label(false)?>
    <?=$form->field($model, 'message')->textarea(['placeholder' => 'Message', 'rows'=>10, 'class' => 'form-input'])->label(false)?>
    <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className())->label(false) ?>
    <?=Html::submitButton('Submit')?>
    <?php ActiveForm::end() ?>
</div>