<?php

use \yii\helpers\Html;
use \app\models\UploadModel;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$this->registerJsFile('js/gallery.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="items">
    <?php if (!empty($images)): ?>
        <?php foreach ($images as $image): ?>
            <?= Html::img('uploads/' . $image->src, ['onclick' => 'showimg(this)', 'srcset'=>UploadModel::imageCrop('uploads/' . $image->src, 400, $image->id, $folder)." 1920w, ".
                                                                                                  UploadModel::imageCrop('uploads/' . $image->src, 500, $image->id, $folder)." 2560w"])?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
