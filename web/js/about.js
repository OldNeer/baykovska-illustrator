$(document).ready(function () {
  resize();
  $(window).resize(function(){
    resize();
  })
})
function resize() {
  if($(window).width() > 768){
  var cntwidth = $(".about-cnt").width() - 5 ;
  var imgwidth = (cntwidth / 2) - 50;
  $("#houses").insertBefore(".content");
} else {
  $(".about-cnt").width($(window).width()-50)
  var cntwidth = $(".about-cnt").width() - 5;
  var imgwidth = (cntwidth);
  $(".description").width(cntwidth);
  $(".description").css("margin-left",0);
  $(".desc-img img").width(imgwidth);
  $(".desc-img img").height(imgwidth);
  $("#houses").width(imgwidth);
  $("#houses").height(imgwidth);
  $("#houses").insertAfter(".description")
}
}
