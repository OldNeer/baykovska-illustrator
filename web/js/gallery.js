$(document).ready(function () {
    $(window).resize(function () {
        if (opn) sizing();
    })
    $(".gallery-bg").click(function () {
        showimg();
    });
    $(".closeGlr").click(function () {
        showimg();
    })
})

function sizing() {
    $(".top").not(".navbuttns img").width($(window).width() / 1.6);
    while ($(".top").height() > $(window).height() - 100) {
        $(".top").width($(".top").width() - 1);
    }
    $(".gallery").width($(".top").width())
    $(".gallery").height($(".top").height())
    $(".gallery").css("left", ($(window).width() / 2) - ($(".gallery").width() + 20) / 2);
    $(".gallery").css("top", ($(window).height() / 2) - (($(".gallery").height() + 20) / 1.9));
    $(".gallery-bg").height($("body").height())
    $(".gallery-bg").width($("body").width())
}

var opn;

function showimg(img) {
    if ($(window).width() >= 768) {
        var allnext = $(img).nextAll();
        var allprev = $(img).prevAll().toArray().reverse();
        $(allprev).clone().appendTo(".images").removeAttr('srcset');
        var top = $(img).clone().appendTo(".images").removeAttr('srcset');
        $(top).addClass("top");
        $(allnext).not(img).clone().appendTo(".images").removeAttr('srcset');
        var indexofopn = $(img).parent().parent().index()
        for (var i = 0; i < indexofopn; i++) {
            $("#content .image:eq(" + i + ")").children().children().clone().appendTo(".images");
        }
        $(".gallery img").removeAttr('onClick');
        openglr();
        sizing();
    } else {
        opn = true;
        openglr();
    }
}

function openglr() {
    opn = opn ? false : true;
    if (opn) {
        $("html *").not(".gallery-bg,.gallery,.gallery *, .navbuttns *, .navbuttns").css("z-index", "-1");
        $("html").css("overflow", "hidden");
        $('.navbuttns').css('display', 'block');
        $('.gallery-bg').css("display", "block");
        $('.gallery').css("display", "block");
    } else {
        $("html *").css("z-index", "auto");
        $("html").css("overflow", "auto");
        $('.gallery-bg').css("display", "none");
        $('.gallery').css("display", "none");
        $('.navbuttns').css('display', 'none');
        $(".images").empty();
    }
}

function previmg() {
    img = $(".gallery img.top");
    if ($(".images img:first").attr("class") == "top") {
        prev = $(".images img:eq(" + ($("#content img").length - 1) + ")");
        $(".desc").text($(prev).attr("desc"));
        prev.addClass("top");
        img.removeClass("top");
    }
    prev = img.prev();
    prev.addClass("top");
    img.removeClass("top");
    sizing();
}

function nextimg() {
    img = $(".gallery img.top");
    next = img.next();
    next.addClass("top");
    img.removeClass("top");
    if (!next.length) {
        next = $(".images img:first");
        next.addClass("top");
    }
    sizing();
}
