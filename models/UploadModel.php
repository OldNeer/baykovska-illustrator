<?php

namespace app\models;

use yii\base\Model;

class UploadModel extends Model
{
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @param $folder
     * @return bool|string
     */
    public function upload($folder)
    {
        if ($this->validate()) {
            if (!file_exists('uploads/' . $folder)) {
                mkdir('uploads/' . $folder, 0777);
            }
            if (!file_exists('uploads/' . $folder)) {
                mkdir('uploads/' . $folder, 0777);
            }
            $files = glob('uploads/' . $folder . '/*');
            $name = count($files);
            $extension = $this->imageFile->extension;
            while (file_exists('uploads/' . $folder . '/' . $name . '.' . $this->imageFile->extension)) {
                $name++;
            }
            $this->imageFile->saveAs('uploads/' . $folder . '/' . $name . '.' . $this->imageFile->extension);
            return $name . '.' . $extension;
        } else {
            return false;
        }
    }

    public static function imageCrop($image, $w, $name, $folder)
    {
        if ($w <= 0) {
            return;
        }
        list($lInitialImageWidth, $lInitialImageHeight, $lImageExtensionId) = getimagesize($image);
        $h = ($lInitialImageHeight * $w) / $lInitialImageWidth;
        header('Cache-Control: public, max-age=3600');
        if (file_exists('cache/' . $folder . '/' . $name . '/' . md5($image . $w . $h) . '.jpeg')) {
            return 'cache/' . $folder . '/' . $name . '/' . md5($image . $w . $h) . '.jpeg';
        }
        $lAllowedExtensions = array(1 => "jpg", 2 => "jpeg", 3 => "png");
        if (!array_key_exists($lImageExtensionId, $lAllowedExtensions)) {
            return false;
        }
        $lImageExtension = $lAllowedExtensions[$lImageExtensionId];
        $func = 'imagecreatefrom' . $lImageExtension;
        $lInitialImageDescriptor = $func($image);
        $lCroppedImageWidth = 0;
        $lCroppedImageHeight = 0;
        $lInitialImageCroppingX = 0;
        $lInitialImageCroppingY = 0;
        if ($w / $h > $lInitialImageWidth / $lInitialImageHeight) {
            $lCroppedImageWidth = floor($lInitialImageWidth);
            $lCroppedImageHeight = floor($lInitialImageWidth * $h / $w);
            $lInitialImageCroppingY = floor(($lInitialImageHeight - $lCroppedImageHeight) / 2);
        } else {
            $lCroppedImageWidth = floor($lInitialImageHeight * $w / $h);
            $lCroppedImageHeight = floor($lInitialImageHeight);
            $lInitialImageCroppingX = floor(($lInitialImageWidth - $lCroppedImageWidth) / 2);
        }
        $lNewImageDescriptor = imagecreatetruecolor($w, $h);
        imagecopyresampled($lNewImageDescriptor, $lInitialImageDescriptor, 0, 0, $lInitialImageCroppingX, $lInitialImageCroppingY, $w, $h, $lCroppedImageWidth, $lCroppedImageHeight);
        if (!file_exists('cache/' . $folder)) {
            mkdir('cache/' . $folder);
        }
        if (!file_exists('cache/' . $folder . '/' . $name)) {
            mkdir('cache/' . $folder . '/' . $name);
        }
        imagejpeg($lNewImageDescriptor, 'cache/' . $folder . '/' . $name . '/' . md5($image . $w . $h) . '.' . $lImageExtension);
        imagedestroy($lNewImageDescriptor);
        return 'cache/' . $folder . '/' . $name . '/' . md5($image . $w . $h) . '.' . $lImageExtension;
    }

}