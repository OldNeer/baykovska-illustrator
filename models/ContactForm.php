<?php
/**
 * Created by PhpStorm.
 * User: Athlon
 * Date: 28.09.2018
 * Time: 15:42
 */

namespace app\models;


use yii\base\Model;
use Yii;

class ContactForm extends Model
{
    public $reCaptcha;
    public $name;
    public $email;
    public $message;
    public $subject;

    public function rules()
    {
        return [
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdUqj0UAAAAAH7_cST4J3e4AIBqnHcpBGZYAzWg', 'uncheckedMessage' => 'Please confirm that you are not a bot.'],
            [['name', 'email', 'message', 'subject'], 'required'],
            [['name', 'email', 'subject'], 'string', 'max' => '255'],
            ['message', 'safe'],
            ['email', 'email'],
        ];
    }
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('irabaykovska@gmail.com')
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject($this->subject)
                ->setHtmlBody('
                <p>Ім\'я: ' . $this->name . '</p>
                <p>Email: ' . $this->email . '</p>
                <p>Повідомлення: <p>'.$this->message.'</p></p>
                <hr>')
                ->send();
            return true;
        }
        return false;
    }
}