<?php

use yii\helpers\Html;
use kartik\file\FileInput;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Publication */

$this->title = 'Загрузка картинок';
$this->params['breadcrumbs'][] = ['label' => 'Publications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="publication-form">
        <?=
        FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'options' => [
                'multiple' => true,
                'class' => 'imageSelect',
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/admin/publication/image-upload']),
                'uploadExtraData' => new JsExpression('function(){return {folder: "publication"}}'),
                'overwriteInitial' => false,
                'showRemove' => true,
            ],
        ]);
        ?>
    </div>

</div>
