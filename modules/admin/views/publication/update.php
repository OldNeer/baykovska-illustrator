<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Publication */

$this->title = 'Описання';
$this->params['breadcrumbs'][] = ['label' => 'Publication', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="publication-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="publication-form">

        <?php $form = ActiveForm::begin(); ?>

        <?=Html::img('/uploads/'.$model->src, ['style' => 'width: 200px; margin: 0 auto; display: block;'])?>
        <?= $form->field($model, 'desctiption')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
