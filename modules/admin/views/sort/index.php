<?php

use kartik\sortable\Sortable;

$this->registerJsVar('folder', $folder);
$this->registerJsFile('/js/sort.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>
<div class="cnt">
    <div class="items">
        <?= Sortable::widget([
            'type' => 'grid',
            'items' => $items,
        ]); ?>
    </div>
    <button class="save btn btn-success">Зберегти</button>
</div>
<script>

</script>