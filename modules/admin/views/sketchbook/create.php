<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = 'Загрузка картинок';
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Загрузка картинок';
?>
<div class="portfolio-create">

    <h1>Загрузка картинок</h1>

    <?= $this->render('_form', [
        'uploadModel' => $uploadModel,
    ]) ?>

</div>
