<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sketchbook';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавити картинки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:80px'],
                'contentOptions' => ['style' => 'text-align: center;'],
            ],
            [
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img('/uploads/' . $model->src, ['width' => '200']);
                },
                'contentOptions' => ['style' => 'text-align: center;'],
            ],

            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width:80px'],
                'contentOptions' => ['style' => 'font-size: 25px; text-align: center;'],
                'template' => '{delete}'
            ],
        ],
    ]); ?>
</div>
