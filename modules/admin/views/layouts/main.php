<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="icon" href="/img/ico.png" type="img/png">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app', 'Portfolio'), 'url' => ['/admin'], 'active' => Yii::$app->controller->id == 'portfolio'],
            ['label' => Yii::t('app', 'Sketchbook'), 'url' => ['/admin/sketchbook'], 'active' => Yii::$app->controller->id == 'sketchbook'],
            ['label' => Yii::t('app', 'Publication'), 'url' => ['/admin/publication'], 'active' => Yii::$app->controller->id == 'publication'],
            ['label' => 'Сортування', 'items' => [
                ['label' => Yii::t('app', 'Portfolio'), 'url' => ['/admin/sort/portfolio']],
                ['label' => Yii::t('app', 'Sketchbook'), 'url' => ['/admin/sort/sketchbook']],
                ['label' => Yii::t('app', 'Publication'), 'url' => ['/admin/sort/publication']]
            ],
                'active' => Yii::$app->controller->id == 'sort',
            ],
            Yii::$app->user->isGuest ? (
            ['label' => Yii::t('app', 'Увійти'), 'url' => ['/user/security/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/user/security/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app', 'Вийти') . '(' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app', 'Головна'), 'url' => '/admin'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
