<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">

    <?=
    FileInput::widget([
        'name' => 'Image',
        'options' => ['accept' => 'image/*'],
        'options' => [
            'multiple' => true,
            'class' => 'imageSelect',
        ],
        'pluginOptions' => [
            'uploadUrl' => Url::to(['/admin/portfolio/image-upload']),
            'uploadExtraData' => new JsExpression('function(){return {folder: "portfolio"}}'),
            'overwriteInitial' => false,
            'showRemove' => true,
        ],
    ]);
    ?>

</div>
