<?php

namespace app\modules\admin\controllers;

use app\models\UploadModel;
use Yii;
use app\models\Sketchbook;
use app\modules\admin\models\SketchbookSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SketchbookController implements the CRUD actions for Sketchbook model.
 */
class SketchbookController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sketchbook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SketchbookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Sketchbook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $uploadModel = new UploadModel();
        return $this->render('create', [
            'uploadModel' => $uploadModel,
        ]);
    }


    /**
     * Deletes an existing Sketchbook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->delImg($model->src);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Sketchbook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sketchbook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sketchbook::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return \yii\web\Response
     */
    public function actionImageUpload()
    {
        $folder = Yii::$app->request->post('folder');
        $uploadModel = new UploadModel();
        $uploadModel->imageFile = UploadedFile::getInstanceByName('Image');
        $name = $uploadModel->upload($folder);
        if(!$uploadModel->hasErrors()){
            $model = new Sketchbook();
            $model->src = $folder . '/' . $name;
            $model->position = Sketchbook::find()->max('position')+1;
            $model->save();
        }
        return $this->asJson([
            'error' => $uploadModel->getErrors('imageFile'),
            'initialPreview' => [
                Html::img('/uploads/' . $folder . '/' . $name, ['class' => 'file-preview-image'])
            ],
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'deleteUrl' => Url::to(['/admin/' . $folder . '/image-delete']),
            'initialPreviewConfig' => [
                0 => [
                    'caption' => $name,
                    'url' => Url::to(['/admin/' . $folder . '/image-delete']),
                    'extra' => ['name' => $name, 'folder' => $folder],
                ],
            ],
            'append' => true,
            'initialPreviewThumbTags' => [

            ],
        ]);
    }

    public function actionImageDelete()
    {
        $name = Yii::$app->request->post('name');
        $folder = Yii::$app->request->post('folder');
        if (file_exists('uploads/' . $folder . '/' . $name)) {
            unlink('uploads/' . $folder . '/' . $name);
        }
        return $this->asJson('OK');
    }

    private function delImg($src)
    {
        if (file_exists('uploads/' . $src)) {
            unlink('uploads/' . $src);
        }
    }
}
