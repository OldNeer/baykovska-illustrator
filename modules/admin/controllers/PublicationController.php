<?php

namespace app\modules\admin\controllers;

use app\models\Publication;
use Yii;
use app\models\UploadModel;
use app\modules\admin\models\PublicationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * PublicationController implements the CRUD actions for Publication model.
 */
class PublicationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Publication models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('position');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Publication model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $uploadModel = new UploadModel();
        return $this->render('create', [
            'uploadModel' => $uploadModel,
        ]);
    }


    /**
     * Deletes an existing Publication model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->delImg($model->src);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Publication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Publication|nrull the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publication::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionImageUpload()
    {
        $folder = Yii::$app->request->post('folder');
        $uploadModel = new UploadModel();
        $uploadModel->imageFile = UploadedFile::getInstanceByName('Image');
        $name = $uploadModel->upload($folder);
        if(!$uploadModel->hasErrors()){
            $model = new Publication();
            $model->src = $folder . '/' . $name;
            $model->position = Publication::find()->max('position')+1;
            $model->save();
        }
        return $this->asJson([
            'error' => $uploadModel->getErrors('imageFile'),
            'initialPreview' => [
                Html::img('/uploads/' . $folder . '/' . $name, ['class' => 'file-preview-image'])
            ],
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'deleteUrl' => Url::to(['/admin/' . $folder . '/image-delete']),
            'initialPreviewConfig' => [
                0 => [
                    'caption' => $name,
                    'url' => Url::to(['/admin/' . $folder . '/image-delete']),
                    'extra' => ['name' => $name, 'folder' => $folder],
                ],
            ],
            'append' => true,
            'initialPreviewThumbTags' => [

            ],
        ]);
    }

    public function actionImageDelete()
    {
        $name = Yii::$app->request->post('name');
        $folder = Yii::$app->request->post('folder');
        if (file_exists('uploads/' . $folder . '/' . $name)) {
            unlink('uploads/' . $folder . '/' . $name);
        }
        return $this->asJson('OK');
    }

    private function delImg($src)
    {
        if (file_exists('uploads/' . $src)) {
            unlink('uploads/' . $src);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

}
