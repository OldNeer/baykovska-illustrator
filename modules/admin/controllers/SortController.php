<?php

namespace app\modules\admin\controllers;


use app\models\Portfolio;
use app\models\Publication;
use app\models\Sketchbook;
use Yii;
use yii\web\Controller;
use yii\helpers\Html;

class SortController extends Controller
{

    public function actionPortfolio()
    {
        $items = [];
        $models = Portfolio::find()->orderBy('position')->asArray()->all();
        foreach ($models as $model) {
            $items[] = '/uploads/' . $model['src'];
        }
        $content = [];
        foreach ($items as $k => $item) {
            $content[] = ['content' => Html::img($item)];
        }
        return $this->render('index', ['items' => $content, 'folder' => 'Portfolio']);
    }

    public function actionSketchbook()
    {
        $items = [];
        $models = Sketchbook::find()->orderBy('src')->all();
        foreach ($models as $model) {
            $items[] = '/uploads/' . $model['src'];
        }
        natsort($items);
        $content = [];
        foreach ($items as $item) {
            $content[] = ['content' => Html::img($item)];
        }
        return $this->render('index', ['items' => $content, 'folder' => 'Sketchbook']);
    }

    public function actionPublication()
    {
        $items = [];
        $models = Publication::find()->orderBy('position')->all();
        foreach ($models as $model) {
            $items[] = '/uploads/' . $model['src'];
        }
        $content = [];
        foreach ($items as $item) {
            $content[] = ['content' => Html::img($item)];
        }
        return $this->render('index', ['items' => $content, 'folder' => 'Publication']);
    }

    public function actionIndex()
    {
        $names = Yii::$app->request->post('names');
        $class = 'app\models\\'.Yii::$app->request->post('class');
        foreach ($names as $k => $name) {
            $model = $class::find()->where(['like', 'src', str_replace('/uploads/', '', $name)])->one();
            if ($model != null) {
                if ($model->position != $k) {
                    $model->position = $k;
                    $model->save();
                }
            }
        }

    }

}